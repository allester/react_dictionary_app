import { useEffect, useState } from 'react'
import axios from 'axios';
import './App.css';
import Container from '@material-ui/core/Container'
import Header from './components/Header/Header';
import Definition from './components/Definition/Definition';
import { withStyles } from '@material-ui/styles';
import { grey }from '@material-ui/core/colors'
import Switch from '@material-ui/core/Switch'

function App() {

  const [word, setWord] = useState('')
  const [meanings, setMeanings] = useState([])
  const [category, setCategory] = useState('en')
  const [lightMode, setLightMode] = useState(false)

  const DarkMode = withStyles({
    switchBase: {
      color: grey[300],
      "&$checked": {
        color: grey[500],
      },
      "&$checked + $track": {
        backgroundColor: grey[500],
      },
    },
    checked: {},
    track: {},
  })(Switch);

  // Will be called whenever a word is typed
  useEffect(() => {

    const dictionaryAPI = async () => {
      try {
        const data = await axios.get(
          `https://api.dictionaryapi.dev/api/v2/entries/${category}/${word}`
        )
        // console.log(data)
  
        setMeanings(data.data)
      } catch (error) {
        console.log(error)
      }
    }

    dictionaryAPI()
  }, [word, category])

  return (
    <div 
      className="App" 
      style={{
        height:'100vh', 
        backgroundColor: lightMode ? '#ffffff' : '#282c34', 
        color: lightMode ? '#000000' : '#ffffff',
        transition: "all 0.5s linear"
        }}
      >
      <Container maxWidth="md" style={{display:'flex',flexDirection:'column', height:'100vh', justifyContent:'space-evenly' }}>
        <div style={{position:'absolute',top:0, right:15, paddingTop:10}}>
          <span>{lightMode ? 'Dark' : 'Light'} Mode</span>
          <DarkMode checked={lightMode} onChange={() => setLightMode(!lightMode)} />
        </div>
        <Header 
          category={category} 
          setCategory={setCategory} 
          word={word} 
          setWord={setWord} 
          lightMode={lightMode}
        />
        { meanings && 
          <Definition 
            word={word} 
            meanings={meanings} 
            category={category} 
            lightMode={lightMode}
          />
        }
      </Container>
    </div>
  );
}

export default App;
