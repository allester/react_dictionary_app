import React from 'react'
import './Definition.css'

const Definition = ({word, meanings, category, lightMode}) => {
    return (
        <div className="meanings">

            {
                meanings[0] && word && category === 'en' && (
                    <audio 
                        src={meanings[0].phonetics[0] && meanings[0].phonetics[0].audio} 
                        style={{backgroundColor:'#ffffff', borderRadius:10}}
                        controls
                    >
                        Your browser doesn't support audio element
                    </audio>
                )
            }

            {
            word === '' ? (
            <span className="subTitle">Start by typing a word in Search</span>
            ) :(
                meanings.map((mean) => {
                    return mean.meanings.map((item) => {
                        return item.definitions.map((def) => {
                            return (
                                <div className='singleMean' style={{backgroundColor: lightMode ? '#3b5360' : '#ffffff', color: lightMode ? '#ffffff' : '#000000'}}>
                                    <strong>{def.definition}</strong>
                                    <hr style={{backgroundColor:'#000000', width:'100%'}} />
                                    {
                                        def.example && (
                                            <span>
                                                <strong>Example: </strong>{def.example}
                                            </span>
                                        )
                                    }
                                    {
                                        def.synonyms && (
                                            <span>
                                                <strong>Synonyms: </strong> {def.synonyms.map((s) => {
                                                    return `${s}, `
                                                })}
                                            </span>
                                        )
                                    }
                                </div>
                            )
                        })
                    })
                })
            )}
        </div>
    )
}

export default Definition